###########
bear-export
###########

Export Bear notes as individual files.

::

   usage: bear-export [-h] [-o DIR] [-v]

   Export from Bear.

   optional arguments:
     -h, --help            show this help message and exit
     -o DIR, --output-directory DIR
                           output directory
     -v, --verbose         show verbose output
