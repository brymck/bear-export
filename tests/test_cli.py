import pathlib
from io import StringIO
import sqlite3

from mockito import mock, unstub, when

import bearexport.cli


def test_cli():
    connection = mock(sqlite3.Connection)
    cursor = mock(sqlite3.Cursor)

    when(sqlite3).connect('/dev/null').thenReturn(connection)
    when(connection).cursor().thenReturn(cursor)
    when(cursor).execute('select ZTITLE, ZUNIQUEIDENTIFIER, ZTEXT from ZSFNOTE').thenReturn([('a', 'b', 'c')])

    path = mock(pathlib.Path)
    output = StringIO()

    when(bearexport.cli).Path('/dev/null').thenReturn(path)
    when(path).joinpath('a.md').thenReturn(path)
    when(path).resolve().thenReturn('/dev/null')
    when(path).open('w').thenReturn(output)

    bearexport.cli.main(['-d', '/dev/null', '-o', '/dev/null', '-v'])

    unstub()
