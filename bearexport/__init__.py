"""
    bear-export
    ~~~~~~~~~~~

    Export from Bear

    :copyright: © 2019 by Bryan Lee McKelvey.
    :license: MIT, see LICENSE for more details.
"""

__version__ = '0.0.1'
