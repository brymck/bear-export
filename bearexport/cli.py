"""
    bearexport.cli
    ~~~~~~~~~~~~~~

    A simple command line application to export from Bear.

    :copyright: © 2019 by Bryan Lee McKelvey.
    :license: MIT, see LICENSE for more details.
"""

import argparse
import logging
import os
import os.path
from pathlib import Path
import re
import sqlite3
import sys
from typing import List
import unicodedata

_default_db_path = os.path.expanduser(
    '~/Library/Group Containers/9K33E3U3T4.net.shinyfrog.bear/Application Data/database.sqlite')


def _slugify(value: str) -> str:
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces to hyphens.
    Remove characters that aren't alphanumerics, underscores, or hyphens.
    Convert to lowercase. Also strip leading and trailing whitespace.
    """
    value = str(value)
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)


def _export(db_path: Path, output_directory: Path):
    logger = logging.getLogger(__name__)
    conn = sqlite3.connect(str(db_path.resolve()))
    c = conn.cursor()
    for (title, uid, text) in c.execute('select ZTITLE, ZUNIQUEIDENTIFIER, ZTEXT from ZSFNOTE'):
        filename = _slugify(title) + '.md'
        fullname = output_directory.joinpath(filename)
        logger.debug(f'Exporting "{title}" to {fullname}')
        with fullname.open('w') as f:
            f.write(text)


def _init_logging(verbose: bool):
    level = logging.DEBUG if verbose else logging.INFO
    logging.basicConfig(format='%(message)s', level=level)


def main(argv: List[str] = sys.argv[1:]):
    parser = argparse.ArgumentParser(description='Export from Bear.')
    parser.add_argument('-d', '--database', default=_default_db_path, type=Path, metavar='PATH',
                        help='path to Bear database')
    parser.add_argument('-o', '--output-directory', default=os.getcwd(), type=Path, metavar='DIR',
                        help='output directory')
    parser.add_argument('-v', '--verbose', action='store_true', help='show verbose output')
    args = parser.parse_args(argv)
    _init_logging(args.verbose)
    _export(args.database, args.output_directory)
